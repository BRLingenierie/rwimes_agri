# use roxygen2:
# https://cran.r-project.org/web/packages/roxygen2/vignettes/rd.html
# run roxygen2::roxygenise() to generate documentation


################################################################################################################
######################################## Extraction data from grid in WIMES ####################################
################################################################################################################

#' Calculate the observed and forecast rainfall/ temperature/ humidity in Cote d'ivoire
#'
#' @export

# wimesInitialization("https://wimes-agri.brl.fr", "dee75f4a-4c4c-417e-9fa3-045fc2134de7")

data_gpm_grid_ci = function() {
  
  
  ################################# 1 - Library loading & date format  ##########################################
  logInfo("STEP 1: Loading libraries")
  library(rwimes)
  library(geojsonR)
  library(rgdal)
  library(raster)
  library(data.table)

  ################################# 2 - Observed grid code declaration #####################################
  logInfo("STEP 2: Observed grid code declaration")
  
  # observed grid code
  gpm_gridP_code = "GPM_P_1day"

  ################################# 3 - Parameters Initialisation  ###############################################
  logInfo("STEP 3: Parameters Initialisation")
 
  # Production date
  prodDate <- as.POSIXlt(Sys.Date(), tz = "UTC")
  
  # Getting site
  
  sites_cum = tryCatch({
    sites = getSites()
    sites_cum = sites[which(substr(sites[,1],1,4) %like% "CUM_"),]
  },
  error = function(e) {
    stop(paste("Error during getSite request : ", e$message))
  })
  
  ################################# 4 - Data extraction per commun   ##############################################
  logInfo("STEP 4: Data extraction per commun")
  
  for (i in seq(along = sites_cum$code)){
    
    # site code
    basinCode = sites_cum[i,1]
    logInfo(paste0("Data extraction of commune: ", basinCode))
    
    
    # Timeserie codes
    or_code = paste0(basinCode, "_PJ")
    
    # commun geometry
    tryCatch({
      basinCoordinates = getGeometry(basinCode)
    },
    error = function(e) {
      stop(paste("Error during geometry reading : ", e$message))
    })
   
    ################
    # observed rainfall
    tryCatch({
      logInfo("Observed rainfall") 
      observedRainfall = CalculateObservedRainfallBasin(gpm_gridP_code, basinCoordinates, prodDate)
      
      # upload the value to WIMES
      logInfo("Upload observed rainfall")
      upsertQuantValue(or_code, as.POSIXct(prodDate, tz = "UTC"), observedRainfall[2])
    },
    error = function(e) {
      logError(e$message)
    }) 
  }
  ############################################ END  ###############################################
  logInfo("END!")
}