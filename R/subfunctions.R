#################################################
#'
#' Calculates rainfall on a watershed from a grid
#'
#' @param tiffFile : name of tiff, used to be downloaded from Wimes
#' @param bv : GDAL polygon
#' 
#' @return rainfall or -9999 if error
#' 
CalculateBVRainfall = function(tiffFile, bv) {
  # Load the tiff from Wimes
  tiff = getGrid(tiffFile)
  
  # Check if file is corrected downloaded
  if (!file.exists(tiff)) {
    logWarning(paste("Can't load", tiffFile))
    return(-9999)
  }
  
  tryCatch(
    {
      # logInfo("Converting tiff file to raster")
      rgdal = readGDAL(tiff, silent = TRUE)
      rainfall_raster = raster(rgdal)
      crs(rainfall_raster) = "+init=epsg:4326"
      
      # logInfo("Rasterizing the basin...")
      raster_bv = rasterize(bv, rainfall_raster, getCover = TRUE, silent=TRUE)
      
      # logInfo("Calculating basin rainfall")
      sum_raster_bv = cellStats(raster_bv, sum)
      if (!is.na(sum_raster_bv) & sum_raster_bv > 0) {
        return(cellStats(raster_bv * rainfall_raster, sum) / sum_raster_bv)
      } else {
        return(-9999)
      }
    },
    error = function(e) {
      logWarning(paste("Error during calculating rainfall calculation :", e$message))
      return(-9999)
    }
  )
}


######################################################
#'
#' Returns the geometry of as site as a GDAL geometry
#'
#' @param basinCode String - Code of basin 
#' 
#' @return site geometry as GDDAL - stop if an error occurs
#' 
getGeometry = function(basinCode) {
  tryCatch(
    {
      logInfo(paste("Getting the geometry of the site ", basinCode, "...", sep = ""))
      
      Rgeom = tryCatch(
        getSite(basinCode),
        error = function(e){
          stop(paste("Error during getSite request : ", e$message))
        }
      )
      
      if (Rgeom$geometry$type != "MultiPolygon") {
        stop(paste("The geometry type of", siteCode, "must be MultiPolygon."))
      }
      
      Rgeom = Rgeom$geometry$coordinates
      nb = length(Rgeom[,,,1])
      coord = lapply(1:nb, FUN = function(x) Rgeom[,,x,])
      
      # Creates the JSON object
      logInfo("Creating the JSON object...")
      init = TO_GeoJson$new()
      json = init$MultiPolygon(list(list(coord)), stringify = TRUE)
      
      # Reads JSON file as GDAL geometry
      logInfo("Reading JSON file as GDAL geometry...")
      bvGeo = readOGR(json$json_dump, "OGRGeoJSON", verbose = FALSE)
      
      return(bvGeo)
    },
    error = function(e){
      stop(paste("Error during getting the geometry of the site : ", e$message))
    }
  )
}

#############################################################
#'
#' Calculates rainfall forecast on a basin 
#'
#' @param gridSiteCode_rf String - code of the timeserie which contains the forecasted rainfall  grids
#' @param basinCoordinates GDAL polygon - Basin geometry
#' @param startDate Date format - start date
#' 
#' @return dataframe (day, daily forecasted rainfall ) - can stop
#'
calculateForecastRainfallBasin = function(gridSiteCode_rf, basinCoordinates, startDate) {

  # gets all forecasted rainfall grid from today
  logInfo(paste("Getting forecast values from ", 
                gridSiteCode_rf, 
                " time series with forecastBeginDateTime = ", 
                startDate, 
                "...", 
                sep = ""))
  
  tiffFiles = getForecastValues(gridSiteCode_rf, forecastBeginDateTime = startDate)
  # loop on all forecast tiff
  rainfallValues = lapply(tiffFiles$value, CalculateBVRainfall, bv = basinCoordinates)
  # convert list to vector
  rainfallValues = unlist(rainfallValues, use.name = FALSE) 
  rainfallDates = tiffFiles$forecastDateTime
  # date - mm/h
  rainfallDataFrame = data.frame(dates = rainfallDates, values = rainfallValues) 
  
  # checks null data
  nullDatas = which(rainfallDataFrame$values == -9999)
  if (length(nullDatas) > length(rainfallDataFrame$values) / 3) {
    stop("Not enough correct data for rainfall forcast")
  }
  
  # table with no null data
  noNullDataFrame = rainfallDataFrame[which(rainfallDataFrame$values != -9999),]
  noNullDataFrame$dates = rev(noNullDataFrame$dates)
  noNullDataFrame$values =  rev(noNullDataFrame$values)
  interpol = approxfun(noNullDataFrame$dates, noNullDataFrame$values, method = "constant", f = 1, ties = "ordered")
  duration = as.numeric(difftime(noNullDataFrame$dates[1], 
                                 noNullDataFrame$dates[length(noNullDataFrame$dates)], 
                                 units="hours")) / 3 * - 1
  
  # to be sure to have a value each 3 hours
  startDate = as.POSIXct(noNullDataFrame$dates[1])
  interpolatedRainfallDates = as.POSIXct(unlist(lapply(0:duration, function(i){ startDate + i * 3 * 3600 } ), use.name = FALSE), 
                                         origin = "1970-01-01")
  interpolatedRainfallValues = unlist(lapply(interpolatedRainfallDates, interpol), use.name = FALSE)
  interpolatedRainfallDataFrame = data.frame(dates=interpolatedRainfallDates, values = interpolatedRainfallValues)
  
  # aggregates hourly rainfall into daily rainfall
  logInfo("Aggragating hourly rainfall into daily rainfall")
  tmpValues = tapply(interpolatedRainfallDataFrame$values, 
                     format(interpolatedRainfallDataFrame$dates, format = "%Y-%m-%d"), 
                     sum)
  dailyRainfall = data.frame(as.Date(names(tmpValues), format = "%Y-%m-%d", tz = "UTC"), as.numeric(tmpValues))
  names(dailyRainfall) = c("Date", "Rainfall (mm/d)")
  dailyRainfall[, 2] = dailyRainfall[, 2] * 3
  
  return(dailyRainfall)
} 

##########################################
#'
#' Calculates observed rainfall  on a basin
#'
#' @param gridSiteCode_ro String - code of the timeserie which contains the observed rainfall grids
#' @param basinCoordinates GDAL polygon - Basin geometry
#' @param startDate Date format - start date
#' 
#' @return vector (date, observed rainfall on basin)
#' 
CalculateObservedRainfallBasin = function(gridSiteCode_ro, basinCoordinates, startDate) {
  beginDateTime = startDate  - 2 * 86400
  endDateTime = startDate
  
  # reads observed rainfall (last rainfall known between d-2 et d)
  logInfo("Getting observed rainfall...")
  tiffFiles = getObservedValues(gridSiteCode_ro, beginDateTime, endDateTime, maxItems = 1)
  
  # gets observed rainfall
  if (length(tiffFiles$value) > 0) {
    # calculates rainfall on basin or polygone
    obsRainfall = CalculateBVRainfall(tiffFiles$value[1], basinCoordinates)
    # return tuple : date, observed rainfall
    obsRainfallDate = c(substr(tiffFiles$observationDateTime, 1, 10), round(obsRainfall, 2))
    
  } else {
    obsRainfallDate = NA
  }
  
  return(obsRainfallDate)
}


#############################################################
#'
#' Calculates forecast relative humidity on a basin 
#'
#' @param gfs_gridRH_code String - code of the timeserie which contains the forecast grids of relative humidity
#' @param basinCoordinates GDAL polygon - Basin geometry
#' @param startDate Date format - start date 
#' @return dataframe (day, rH) - can stop
#'
calculateRelativeHumidityBasin = function(gfs_gridRH_code, basinCoordinates, startDate) {
  
  # gets all forecast Rh grid from today
  logInfo(paste("Getting forecast values from ", 
                gfs_gridRH_code, 
                " time series with forecastBeginDateTime = ", 
                startDate, 
                "...", 
                sep = ""))
  tiffFiles = getForecastValues(gfs_gridRH_code, forecastBeginDateTime = startDate)
  
  # loop on all forecast tiff
  rHValues = lapply(tiffFiles$value, CalculateBVRainfall, bv = basinCoordinates)
  # convert list to vector
  rHValues = unlist(rHValues, use.name = FALSE) 
  rHDates = tiffFiles$forecastDateTime
  # date - %
  rHDataFrame = data.frame(dates = rHDates, values = rHValues) 
  
  # checks null data
  nullDatas = which(rHDataFrame$values == -9999)
  if (length(nullDatas) > length(rHDataFrame$values) / 2) {
    stop("Not enough correct data for forcast relative humidity")
  }
 
  # table with no null data
  noNullDataFrame = rHDataFrame[which(rHDataFrame$values != -9999),]
  noNullDataFrame$dates = rev(noNullDataFrame$dates)
  noNullDataFrame$values =  rev(noNullDataFrame$values)
  interpol = approxfun(noNullDataFrame$dates, noNullDataFrame$values, method = "constant", f = 1, ties = "ordered")
  duration = as.numeric(difftime(noNullDataFrame$dates[1], 
                                 noNullDataFrame$dates[length(noNullDataFrame$dates)], 
                                 units="hours")) / 3 * - 1
  
  # to be sure to have a value each 3 hours
  startDate = as.POSIXct(noNullDataFrame$dates[1])
  interpolatedRhDates = as.POSIXct(unlist(lapply(0:duration, function(i){ startDate + i * 3 * 3600 } ), use.name = FALSE), 
                                         origin = "1970-01-01")
  interpolatedRhValues = unlist(lapply(interpolatedRhDates, interpol), use.name = FALSE)
  interpolatedRhDataFrame = data.frame(dates=interpolatedRhDates, values = interpolatedRhValues)
  
  # Calculating daily relative humidity
  logInfo("Calculating daily relative humidity")
  tmpValues = tapply(interpolatedRhDataFrame$values, 
                     format(interpolatedRhDataFrame$dates, format = "%Y-%m-%d"), 
                     mean)
  dailyRh = data.frame(as.Date(names(tmpValues), format = "%Y-%m-%d", tz = "UTC"), as.numeric(tmpValues))
  names(dailyRh) = c("Date", "Rh (%)")

  return(dailyRh) 
}


#############################################################
#'
#' Calculates forecast temperature min on a basin 
#'
#' @param gfs_gridTMIN_code String - code of the timeserie which contains the forecast grids of temperature min
#' @param basinCoordinates GDAL polygon - Basin geometry
#' @param startDate Date format - start date 
#' @return dataframe (day, tMin) - can stop
#'
calculateTemperatureMinBasin = function(gfs_gridTMIN_code, basinCoordinates, startDate) {
  
  # gets all forecast tMin grid from today
  logInfo(paste("Getting forecast values from ", 
                gfs_gridTMIN_code, 
                " time series with forecastBeginDateTime = ", 
                startDate, 
                "...", 
                sep = ""))
  tiffFiles = getForecastValues(gfs_gridTMIN_code, forecastBeginDateTime = startDate)
  
  # loop on all forecast tiff
  tMinValues = lapply(tiffFiles$value, CalculateBVRainfall, bv = basinCoordinates)
  # convert list to vector
  tMinValues = unlist(tMinValues, use.name = FALSE) 
  tMinDates = tiffFiles$forecastDateTime
  # date - °C
  tMinDataFrame = data.frame(dates = tMinDates, values = tMinValues) 
  
  # checks null data
  nullDatas = which(tMinDataFrame$values == -9999)
  if (length(nullDatas) > length(tMinDataFrame$values) / 2) {
    stop("Not enough correct data for forcast temperature")
  }
  
  # table with no null data
  noNullDataFrame = tMinDataFrame[which(tMinDataFrame$values != -9999),]
  noNullDataFrame$dates = rev(noNullDataFrame$dates)
  noNullDataFrame$values =  rev(noNullDataFrame$values)
  interpol = approxfun(noNullDataFrame$dates, noNullDataFrame$values, method = "constant", f = 1, ties = "ordered")
  duration = as.numeric(difftime(noNullDataFrame$dates[1], 
                                 noNullDataFrame$dates[length(noNullDataFrame$dates)], 
                                 units="hours")) / 3 * - 1
  
  # to be sure to have a value each 3 hours
  startDate = as.POSIXct(noNullDataFrame$dates[1])
  interpolatedtMinDates = as.POSIXct(unlist(lapply(0:duration, function(i){ startDate + i * 3 * 3600 } ), use.name = FALSE), 
                                   origin = "1970-01-01")
  interpolatedtMinValues = unlist(lapply(interpolatedtMinDates, interpol), use.name = FALSE)
  interpolatedtMinDataFrame = data.frame(dates=interpolatedtMinDates, values = interpolatedtMinValues)
  
  # Calculating daily temperature min
  logInfo("Calculating daily temperature min")
  tmpValues = tapply(interpolatedtMinDataFrame$values, 
                     format(interpolatedtMinDataFrame$dates, format = "%Y-%m-%d"), 
                     min)
  dailytMin = data.frame(as.Date(names(tmpValues), format = "%Y-%m-%d", tz = "UTC"), as.numeric(tmpValues))
  names(dailytMin) = c("Date", "tMin (°C)")
  
  return(dailytMin) 
}

#############################################################
#'
#' Calculates forecast temperature max on a basin 
#'
#' @param gfs_gridTMax_code String - code of the timeserie which contains the forecast grids of temperature Max
#' @param basinCoordinates GDAL polygon - Basin geometry
#' @param startDate Date format - start date 
#' @return dataframe (day, tMax) - can stop
#'
calculateTemperatureMaxBasin = function(gfs_gridTMax_code, basinCoordinates, startDate) {
  
  # gets all forecast tMax grid from today
  logInfo(paste("Getting forecast values from ", 
                gfs_gridTMax_code, 
                " time series with forecastBeginDateTime = ", 
                startDate, 
                "...", 
                sep = ""))
  tiffFiles = getForecastValues(gfs_gridTMax_code, forecastBeginDateTime = startDate)
  
  # loop on all forecast tiff
  tMaxValues = lapply(tiffFiles$value, CalculateBVRainfall, bv = basinCoordinates)
  # convert list to vector
  tMaxValues = unlist(tMaxValues, use.name = FALSE) 
  tMaxDates = tiffFiles$forecastDateTime
  # date - °C
  tMaxDataFrame = data.frame(dates = tMaxDates, values = tMaxValues) 
  
  # checks null data
  nullDatas = which(tMaxDataFrame$values == -9999)
  if (length(nullDatas) > length(tMaxDataFrame$values) / 2) {
    stop("Not enough correct data for forcast temperature")
  }
  
  # table with no null data
  noNullDataFrame = tMaxDataFrame[which(tMaxDataFrame$values != -9999),]
  noNullDataFrame$dates = rev(noNullDataFrame$dates)
  noNullDataFrame$values =  rev(noNullDataFrame$values)
  interpol = approxfun(noNullDataFrame$dates, noNullDataFrame$values, method = "constant", f = 1, ties = "ordered")
  duration = as.numeric(difftime(noNullDataFrame$dates[1], 
                                 noNullDataFrame$dates[length(noNullDataFrame$dates)], 
                                 units="hours")) / 3 * - 1
  
  # to be sure to have a value each 3 hours
  startDate = as.POSIXct(noNullDataFrame$dates[1])
  interpolatedtMaxDates = as.POSIXct(unlist(lapply(0:duration, function(i){ startDate + i * 3 * 3600 } ), use.name = FALSE), 
                                     origin = "1970-01-01")
  interpolatedtMaxValues = unlist(lapply(interpolatedtMaxDates, interpol), use.name = FALSE)
  interpolatedtMaxDataFrame = data.frame(dates=interpolatedtMaxDates, values = interpolatedtMaxValues)
  
  # Calculating daily temperature max
  logInfo("Calculating daily temperature max")
  tmpValues = tapply(interpolatedtMaxDataFrame$values, 
                     format(interpolatedtMaxDataFrame$dates, format = "%Y-%m-%d"), 
                     max)
  dailytMax = data.frame(as.Date(names(tmpValues), format = "%Y-%m-%d", tz = "UTC"), as.numeric(tmpValues))
  names(dailytMax) = c("Date", "tMax (°C)")
  
  return(dailytMax) 
}