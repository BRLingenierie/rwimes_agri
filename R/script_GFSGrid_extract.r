# use roxygen2:
# https://cran.r-project.org/web/packages/roxygen2/vignettes/rd.html
# run roxygen2::roxygenise() to generate documentation


################################################################################################################
######################################## Extraction data from grid in WIMES ####################################
################################################################################################################

#' @export
testJWI = function() {
  cat("toto")
  print("titi")
}

#' Calculate the observed and forecast rainfall/ temperature/ humidity in Cote d'ivoire
#'
#' @export

# wimesInitialization("https://wimes-agri.brl.fr", "dee75f4a-4c4c-417e-9fa3-045fc2134de7")

data_gfs_grid_ci = function() {

  ################################# 1 - Library loading & date format  ##########################################
  logInfo("STEP 1: Loading libraries")
  library(rwimes)
  library(geojsonR)
  library(rgdal)
  library(raster)
  library(zoo)
  library(data.table)

  ################################# 2 - Forcasted grid code declaration #####################################
  logInfo("STEP 2: Forcasted grid code declaration")
  
  # forcasted grid code
  gfs_gridTMIN_code = "GFS_GRID_FC_TMIN_3H"
  gfs_gridTMAX_code = "GFS_GRID_FC_TMAX_3H"
  gfs_gridRH_code = "GFS_GRID_FC_RH_3H"
  gfs_gridP_code = "GFS_GRID_FC_P_3H"
  
  ################################# 3 - Parameters Initialisation  ###############################################
  logInfo("STEP 3: Parameters Initialisation")

  # Production date
  prodDate <- as.POSIXlt(Sys.Date(), tz = "UTC")
  
  # Getting site
  
  sites_cum = tryCatch({
    sites = getSites()
    sites_cum = sites[which(substr(sites[,1],1,4) %like% "CUM_"),]
  },
  error = function(e) {
    stop(paste("Error during getSite request : ", e$message))
  })
  
  ################################# 4 - Data extraction per commun   ##############################################
  logInfo("STEP 4: Data extraction per commun")
  
  for (i in seq(along = sites_cum$code)){
    # site code
    basinCode = sites_cum[i,1]
    logInfo(paste0("Data extraction of commune: ", basinCode))
    
    # Timeserie codes
    tMin_code <- paste0(basinCode, "_FTMIN")
    tMax_code <- paste0(basinCode , "_FTMAX")
    rh_code <- paste0(basinCode , "_FRH")
    fr_code <- paste0(basinCode , "_FPJ")
    
    # commun geometry
    tryCatch({
      basinCoordinates = getGeometry(basinCode)
    },
    error = function(e) {
      stop(paste("Error during geometry reading : ", e$message))
    })
    
    ################
    # forcast rainfall
    tryCatch({
      logInfo("STEP 4.1 - Forecast rainfall") 
      forecastRainfall = calculateForecastRainfallBasin(gfs_gridP_code, basinCoordinates, prodDate)
    
      # upload the value to WIMES
      logInfo("Upload forecast rainfall")
      upsertQuantForecastValues(data.frame(timeSeriesCode = fr_code, 
                                           productionDateTime = prodDate,
                                           forecastDateTime = as.POSIXct(forecastRainfall[,1], tz = "UTC"),
                                           value = forecastRainfall[,2]))
    },
    error = function(e) {
      logError(e$message)
    }) 
    
    ################
    # forecast relative humidity
    tryCatch({
      logInfo("STEP 4.2 - Forecast relative humidity") 
      forcastRh = calculateRelativeHumidityBasin(gfs_gridRH_code, basinCoordinates, prodDate)
  
      # upload the value to WIMES
      logInfo("upload Forecast relative humidity")
      upsertQuantForecastValues(data.frame(timeSeriesCode = rh_code, 
                                           productionDateTime = prodDate,
                                           forecastDateTime = as.POSIXct(forcastRh[, 1], tz = "UTC"),
                                           value = forcastRh[ , 2]))
    },
    error = function(e) {
      logError(e$message)
    }) 
    ################
    # forecast temperature min
    tryCatch({
      logInfo("STEP 4.3 - Forecast temperature min") 
      
      forcasttMin = calculateTemperatureMinBasin(gfs_gridTMIN_code, basinCoordinates, prodDate)
      
      # upload the value to WIMES
      logInfo("upload Forecast temperature min")
      upsertQuantForecastValues(data.frame(timeSeriesCode = tMin_code, 
                                           productionDateTime = as.POSIXct(prodDate, tz = "UTC"),
                                           forecastDateTime = as.POSIXct(forcasttMin[, 1], tz = "UTC"),
                                           value = forcasttMin[ , 2]))
    },
    error = function(e) {
      logError(e$message)
    }) 
    ################
    # forecast temperature max
    tryCatch({
      logInfo("STEP 4.4 - Forecast temperature max") 
      
      forcasttMax = calculateTemperatureMaxBasin(gfs_gridTMAX_code, basinCoordinates, prodDate)
  
      # upload the value to WIMES
      logInfo("upload Forecast temperature max")
      upsertQuantForecastValues(data.frame(timeSeriesCode = tMax_code, 
                                             productionDateTime = as.POSIXct(prodDate, tz = "UTC"),
                                             forecastDateTime = as.POSIXct(forcasttMax[, 1], tz = "UTC"),
                                             value = forcasttMax[ , 2]))
    },
    error = function(e) {
      logError(e$message)
    }) 
  }
  ############################################ END  ###############################################
  logInfo("END!")
}